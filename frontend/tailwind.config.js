module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        "primary-color": {
          DEFAULT:"#132948",  
          100:"#F0FFFF",
          200:"#E9967A",
          300:"##7FFFD4"
        },
      },
      fontSize: {
        placeHolder: "14px",
      },

    },
  
   
  },
  plugins: [],
}