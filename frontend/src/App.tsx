import React from 'react'
import './App.css'
import Layout from './pages/Auth/Layout'
import Login from './pages/Auth/Login'
import AuthRouter from './routes/AuthRoutes'
import { useDispatch, useSelector } from 'react-redux'
import { loginFailure, loginSuccess } from './store/slice/authSlice'
import PublicRouter from './routes/PublicRoutes'

const App = () => {

  const {isLoggedIn} = useSelector((state:any)=>state.auth)

  console.log(isLoggedIn)

  const dispatch  = useDispatch()

  React.useEffect(()=>{
    if(localStorage.getItem('userInfo')){
      let userInfo = localStorage.getItem('userInfo')
      const userData = userInfo ? JSON.parse(userInfo) : ''

      dispatch(loginSuccess(userData))
    }else{
      dispatch(loginFailure())
    }
  },[])
  return (
    isLoggedIn ||   localStorage.getItem('userInfo') ? <PublicRouter /> : <AuthRouter />
  )
}

export default App
