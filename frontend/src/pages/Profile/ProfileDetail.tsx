import React, { useState } from 'react'
import Card from '../common/Card'
import axios from 'axios'
import {
  BASE_URL,
  GET_ALL_FRIENDS,
  GET_ALL_USERS,
  USER_API,
} from '../../utils/constant'
import { Select } from 'antd'
import Swal from 'sweetalert2'
import { FaTrash } from "react-icons/fa";

const ProfileDetail: React.FC = () => {
  let userInfo = localStorage.getItem('userInfo')
  const userData = userInfo ? JSON.parse(userInfo) : ''

  const [user, setUser] = useState([])
  const [friendList, setFriendList] = useState([])
  const [filteredTempArray, setFilterTempArray] = useState([])
  const getFriendList = async () => {
    axios
      .get(`${BASE_URL}${USER_API}${GET_ALL_FRIENDS}/${userData._id}`)
      .then((res) => {
        if (res.status === 200) {
          console.log(res?.data?.data?.friend)
          setFriendList(res?.data?.data?.friend ? res?.data?.data?.friend : [])

          // setFriend()
        } else {
          Swal.fire('error', 'Something went wrong', 'error')
        }
      })
  }

  React.useEffect(() => {
    getFriendList()
      .then(() => {
        // fetch all users
        return axios.get(`${BASE_URL}${USER_API}${GET_ALL_USERS}`)
      })
      .then((res) => {
        if (res.status === 200) {
          const tempArray = res?.data?.data.map((item) => {
            return {
              ...item,
              label: item.name,
              value: item._id,
            }
          })

          setUser(tempArray)
        } else {
          Swal.fire('error', 'Something went wrong', 'error')
        }
      })
      .catch((error) => {
        console.error('Error:', error)
        Swal.fire('error', 'Something went wrong', 'error')
      })
  }, [])
  const onSearch = (value: string) => {
    console.log('search:', value)
  }

  // Filter `option.label` match the user type `input`
  const filterOption = (
    input: string,
    option?: { label: string; value: string },
  ) => (option?.label ?? '').toLowerCase().includes(input.toLowerCase())

  const [showSelect, setShowSelect] = useState(true)

  const onChange = async (value: string) => {
    const filterAddedUser =await friendList.filter((item, index) => {
      return item.friendId._id == value
    })

    if (filterAddedUser.length > 0 || userData?._id == value) {
        Swal.fire('error', 'User already added and cannot add yourself', 'error')
    } else {
      setShowSelect(false)
      const dataToSend = {
        userId: userData?._id,
        friendId: value,
      }

      const data = await axios.post(
        `${BASE_URL}${USER_API}addFriend`,
        dataToSend,
      )

      setShowSelect(true)

      if (!data.data?.error) {
        Swal.fire('success', 'Successfully added friend', 'success')
        getFriendList()
      }
    }
  }

  const removeFriend = async (friendId: string, name: string) => {
    const dataToSend = {
      userId: userData?._id,
      friendId: friendId,
    }

    const data = await axios.post(
      `${BASE_URL}${USER_API}removeFriend`,
      dataToSend,
    )

    if (!data.data?.error) {
      Swal.fire('success', 'Successfully remove friend', 'success')
      getFriendList()
    }

    // dispatch(getSenderId(friendId))
    // getChat(friendId, name)
  }

  return (
    <div className="w-6/6 ">
      <Card style={{ width: '100%', margin: 0 }}> Profile Details</Card>

      <div className="flex   ">
        <div className="w-1/2 ">
          <Card style={{ marginTop: 5, width: '98%' }}>{userData.name}</Card>
        </div>

        <div className="w-1/2">
          <Card style={{ marginTop: 5, width: '98%' }}>{userData.email}</Card>
        </div>
      </div>
      {showSelect && (
        <Select
          showSearch
          placeholder="Add a friend"
          optionFilterProp="children"
          style={{ width: '99%', marginLeft: 10, marginTop: 5 }}
          onChange={onChange}
          onSearch={onSearch}
          filterOption={filterOption}
          options={user}
          allowClear
        />
      )}
      <br />

      <br />

      <div className="flex w-[100%] justify-center flex-wrap">
        {friendList?.map((item: any) => (
          <>
            <div className="w-3/4">
              <Card
                style={{
                  background: '#7FFFD4',
                  width: '100%',
                  marginLeft: 10,
                  marginTop: 5,
                  borderRadius: 4,
                  marginBottom: 2,
                  cursor: 'pointer',
                }}
               
              >
                {item?.friendId?.name}
              </Card>
            </div>

            <div className="w-16  px-3">
              <Card
                style={{
                  background: 'red',
                  width: '100%',
                  marginLeft: 10,
                  marginTop: 5,
                  borderRadius: 4,
                  marginBottom: 2,
                  cursor: 'pointer',
                  color: 'wheat',
                }}
                onClick={() => {
                  removeFriend(item?.friendId?._id, item?.friendId?.name)
                }}
              >
                <FaTrash size={22}/>
              </Card>
            </div>
          </>
        ))}
      </div>
    </div>
  )
}

export default ProfileDetail
