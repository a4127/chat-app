import React, { ReactNode } from 'react'

interface LayoutProps {
  children: ReactNode
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <div className="">
      <h1 className="bg-yellow-600 font-bold text-2xl text-center">
        Welcome to Chat APP
      </h1>

      <div className="bg-grey-300 h-[calc(100vh-32px)] flex items-center justify-center">
        {children}
      </div>
    </div>
  )
}

export default Layout
