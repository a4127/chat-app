import axios from 'axios'
import React from 'react'
import { useNavigate } from 'react-router'
import * as Yup from 'yup'
import { AUTH_API, BASE_URL } from '../../utils/constant'
import Button from '../common/Button'
import { useFormik } from 'formik'
import Card from '../common/Card'
import { useLocation } from 'react-router'
import Swal from 'sweetalert2'


const OtpVerification: React.FC = () => {
  const navigate = useNavigate()

  const location  = useLocation()


  const validationSchema = Yup.object().shape({
    otp: Yup.string()
   
      .required('Otp is required'),
  })

  const onHandleSubmitForm = async (values: any) => {
    console.log(values)
    const dataWithEmail = {email:location.state.email,...values}

    try {
      const data = await axios.post(
        `${BASE_URL}${AUTH_API}verify-otp`,
        dataWithEmail,
      )

      const res = await data?.data


      if (data.status !==200) {
        Swal.fire({
          text:res.message!,
          icon: "error",
          timer: 1500
        })
      } else {
        Swal.fire({
          text:res.message!,
          icon: "success",
          timer: 1500
        }).then(()=>{
          navigate('/')
        });
      }
    } catch (error) {
      Swal.fire({
        text:"Something went wrong",
        icon: "error"
      });
    }
  }

  const formik = useFormik({
    initialValues: {
      otp: '',
    },
    validationSchema: validationSchema,
    onSubmit: onHandleSubmitForm,
  })

  return (
    <Card>
      <form className="flex flex-col  w-[100%]" onSubmit={formik.handleSubmit}>
        <h2 className="text-center  pb-4 font-bold text-2xl text-primary-color">
          {' '}
          OTP{' '}
        </h2>
        <input
          type="number"
          value={formik.values.otp}
          name="otp"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          placeholder="Please enter the otp .. "
          className="rounded-[4px] p-2 mb-1 focus:ring  text-placeHolder"
        />

        {formik.errors.otp && formik.touched.otp && (
          <span className="text-[#FF8585] text-sm">{formik.errors.otp}</span>
        )}

        <br />
        <Button title="Verify" type={'submit'} />
        <Button
          title="Login"
          onClick={() => {
            navigate('/')
          }}
          style={{ background: 'orange' }}
        />
      </form>
    </Card>
  )
}

export default OtpVerification
