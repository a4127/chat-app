import React from 'react'
import { Link } from 'react-router-dom'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import axios from 'axios'
import { AUTH_API, BASE_URL, LOGIN } from '../../utils/constant'
import Button from '../common/Button'
import Card from '../common/Card'
import { useNavigate } from 'react-router'
import { useDispatch } from 'react-redux'
import { loginSuccess } from '../../store/slice/authSlice'
import Swal from 'sweetalert2'

const Login: React.FC = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .email('Please enter valid email')
      .required('Email is a required field!'),
    password: Yup.string()
      .required('Please enter password')
      .min(4, 'Password must be at least 4 characters'),
  })

  const onHandleSubmitForm = async (values: any) => {

    try {
      const data = await axios.post(`${BASE_URL}${AUTH_API}${LOGIN}`, values)
      const res = await data?.data?.data

      if (!data?.data?.error) {
        localStorage.setItem('userInfo', JSON.stringify(res))
        Swal.fire({
          icon: 'success',
          // title: "Oops...",
          text: 'You have been logged in successfully',
          timer: 1500,
        }).then(() => {
          dispatch(loginSuccess(''))
         navigate('/')
        })

        console.log('this is data' , data)
      }
    } catch (error:any) {



     if(error.response.status == 400 || error.response.status == 401)   {

      const showValidationError = error.response.status == 401 ? error?.response?.data?.errors[0].constraints.error : error?.response?.data?.message

      Swal.fire({
        icon: 'error',
        text:showValidationError,
        timer: 1500,
      })
    }else{
      Swal.fire({
        icon: 'error',
        text:'Something went wrong',
        timer: 1500,
      })
    }
     }

  
  }
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: validationSchema,
    onSubmit: onHandleSubmitForm,
  })



  return (
    <Card>

      
      <form className="flex flex-col w-[100%]" onSubmit={formik.handleSubmit}>
        <h2 className="text-center font-bold text-2xl "> Login</h2>
        <label className="text-primary-color">Email</label>
        <input
          type="text"
          placeholder="Enter E-mail"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.email}
          name="email"
          className="rounded-[4px] p-2 focus:ring  text-placeHolder"
        />

        {formik.errors.email && formik.touched.email && (
          <span className="text-[#FF8585] text-sm">{formik.errors.email}</span>
        )}

        <label className="text-primary-color justify-between flex mt-2">
          Password
        </label>
        <input
          type="password"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.password}
          name="password"
          placeholder="Enter Password"
          className="rounded-[4px] p-2   focus:ring text-placeHolder"
        />

        {formik.errors.password && formik.touched.password && (
          <span className="text-[#FF8585] text-sm">
            {formik.errors.password}
          </span>
        )}

        <br />

        <Button title="Submit" disabled={!formik.isValid || !formik.dirty}  style={{opacity: !formik.isValid || !formik.dirty ? 0.5 :1}}/>

        <br />



        <div className='flex flex-row justify-between'>
          <Link to={'/sign-up'}>Don't have a Accoun't ?</Link>

          <Link to={'/forgot-password'}>Forgot Password ?</Link>
        </div>
      </form>
    </Card>
  )
}

export default Login
