import axios from 'axios'
import React from 'react'
import { useNavigate } from 'react-router'
import * as Yup from 'yup'
import { AUTH_API, BASE_URL } from '../../utils/constant'
import Button from '../common/Button'
import { useFormik } from 'formik'
import Card from '../common/Card'
import Swal from 'sweetalert2'
import { FaLeftLong } from 'react-icons/fa6'

const ForgotPassword: React.FC = () => {
  const navigate = useNavigate()


  const BackIcon = () => {
    return (
      <>
        <div className='flex align-middle justify-center'><FaLeftLong  className='mt-1 mr-2'/> Back to login</div>
      </>
    )
  }


  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .email('Please enter valid email')
      .required('Email is a required field!'),
  })

  const onHandleSubmitForm = async (values: any) => {
    try {
      const data = await axios.post(
        `${BASE_URL}${AUTH_API}/forgot-password`,
        values,
      )
      
    } catch (error:any) {
      if(error.response.status == 400 || error.response.status == 401)   {

        const showValidationError = error.response.status == 401 ? error?.response?.data?.errors[0].constraints.error : error?.response?.data?.message
  
        Swal.fire({
          icon: 'error',
          text:showValidationError,
          timer: 1500,
        })
      }else{
        Swal.fire({
          icon: 'error',
          text:'Something went wrong',
          timer: 1500,
        })
      }

    }
  }

  const formik = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema: validationSchema,
    onSubmit: onHandleSubmitForm,
  })

  return (
    <Card>
      <form className="flex flex-col  w-[100%]" onSubmit={formik.handleSubmit}>
        <h2 className="text-center  pb-4 font-bold text-2xl text-primary-color">
          {' '}
          Email{' '}
        </h2>
        <input
          type="text"
          value={formik.values.email}
          name="email"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          placeholder="Enter E-mail"
          className="rounded-[4px] p-2 mb-1 focus:ring  text-placeHolder"
        />

        {formik.errors.email && formik.touched.email && (
          <span className="text-[#FF8585] text-sm">{formik.errors.email}</span>
        )}

        <br />
          <div className='flex justify-between'>

          <Button title="Send Mail" type={'submit'} disabled={!formik.isValid || !formik.dirty}  style={{opacity: !formik.isValid || !formik.dirty ? 0.5 :1 , width:"48%"}}/>


          <Button
            title={<BackIcon />}
            onClick={() => {
              navigate('/') 
            }}
            style={{ background: 'orange' , width:"48%" }}
          />
          </div>
      </form>
    </Card>
  )
}

export default ForgotPassword
