import { useFormik } from 'formik'
import React, { useEffect, useState } from 'react'
import * as Yup from 'yup'
import { Link, useParams } from 'react-router-dom'
import { AUTH_API, BASE_URL } from '../../utils/constant'
import axios from 'axios'
import Card from '../common/Card'
import Button from '../common/Button'
import Swal from 'sweetalert2'
import { useNavigate } from 'react-router'

const PasswordReset = () => {
    const navigate = useNavigate()

  let { id } = useParams()

  const onHandleSubmitForm = async (values: any) => {
    const dataToSend = {
      id: id,
      password: values.password,
    }

    try {
      const data = await axios.post(
        `${BASE_URL}${AUTH_API}password-reset`,
        dataToSend,
      )
      const res = await data?.data?.data

      
      if (!data?.data?.error) {
        localStorage.setItem('userInfo', JSON.stringify(res))
        Swal.fire({
          icon: 'success',
          // title: "Oops...",
          text: 'Password updated successfully',
          timer: 1500,
        }).then(() => {
         navigate('/')
        })
  
      }

    } catch (error:any) {

        if(error.response.status == 400 || error.response.status == 401)   {

            const showValidationError = error.response.status == 401 ? error?.response?.data?.errors[0].constraints.error : error?.response?.data?.message
      
            Swal.fire({
              icon: 'error',
              text:showValidationError,
              timer: 1500,
            })
          }else{
            Swal.fire({
              icon: 'error',
              text:'Something went wrong',
              timer: 1500,
            })
          }

    }
  }

  const formik = useFormik({
    initialValues: {
      password: '',
      confirmPassword: '',
    },
    validationSchema: Yup.object({
      password: Yup.string().required('Password is required'),
      confirmPassword: Yup.string()
        .oneOf([Yup.ref('password')], 'Password should match')
        .required('Confirm password is required'),
    }),
    onSubmit: onHandleSubmitForm,
  })

  return (
    <Card>
      <form className="flex flex-col w-[100%]" onSubmit={formik.handleSubmit}>
        <label className="text-primary-color mt-5">Password</label>
        <input
          type="text"
          placeholder="Password"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.password}
          name="password"
          className="rounded-[4px] p-2 focus:ring  text-placeHolder"
        />

        {formik.errors.password && formik.touched.password && (
          <span className="text-[#FF8585] text-sm">
            {formik.errors.password}
          </span>
        )}

        <label className="text-primary-color mt-3">Confirm Password</label>
        <input
          type="password"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.confirmPassword}
          name="confirmPassword"
          placeholder="Confirm Password"
          className="rounded-[4px] p-2 focus:ring text-placeHolder"
        />

        {formik.errors.confirmPassword && formik.touched.confirmPassword && (
          <span className="text-[#FF8585] text-sm">
            {formik.errors.confirmPassword}
          </span>
        )}
        <Button
          title="Confirm"
          type={'submit'}
          disabled={!formik.isValid || !formik.dirty}
          style={{
            opacity: !formik.isValid || !formik.dirty ? 0.5 : 1,
          }}
        />
      </form>
    </Card>
  )
}

export default PasswordReset
