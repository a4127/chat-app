import React from 'react'
import { useNavigate } from 'react-router'
import { Link } from 'react-router-dom'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import axios from 'axios'
import { AUTH_API, BASE_URL } from '../../utils/constant'
import Card from '../common/Card'
import Button from '../common/Button'
import { useRef } from 'react'
import Swal from 'sweetalert2'
import { FaLeftLong } from 'react-icons/fa6'

interface formValues {
  email: string
  name: string
  password: string
}

const SignUp: React.FC = () => {
  const navigate = useNavigate()
  const inputRef = useRef<any>(null)

  const validationSchema = Yup.object().shape({
    name: Yup.string().required('Please enter the name!'),

    email: Yup.string()
      .email('Please enter valid email')
      .required('Email is a required field!'),

    password: Yup.string()
      .required('Please enter password!')
      .min(4, 'Password must be at least 4 characters'),
  })

  const onHandleSubmitForm = async (values: formValues) => {
    try {
      const data = await axios.post(`${BASE_URL}${AUTH_API}create`, values)
      const res = await data?.data

      if (data?.status !== 201) {
        Swal.fire({
          text: res.message!,
          icon: 'error',
        })
      } else if (data?.status === 201) {
        Swal.fire({
          text: res.message!,
          icon: 'success',
          timer: 1500,
        }).then(() => {
          navigate('/verify-otp', { state: { email: res.data } })
        })
      }
    } catch (error) {
      if (error.response.status == 400 || error.response.status == 401) {
        const showValidationError =
          error.response.status == 401
            ? error?.response?.data?.errors[0].constraints.error
            : error?.response?.data?.message

        Swal.fire({
          icon: 'error',
          text: showValidationError,
          timer: 1500,
        })
      } else {
        Swal.fire({
          icon: 'error',
          text: 'Something went wrong',
          timer: 1500,
        })
      }
    }
  }

  const formik = useFormik({
    initialValues: {
      name: '',
      password: '',
      email: '',
    },
    validationSchema: validationSchema,
    onSubmit: onHandleSubmitForm,
  })

  const BackIcon = () => {
    return (
      <>
        <div className='flex align-middle justify-center'><FaLeftLong  className='mt-1 mr-2'/> Back to login</div>
      </>
    )
  }
  return (
    <Card>
      <form className="flex flex-col w-[100%]" onSubmit={formik.handleSubmit}>
        <h2 className="text-center  pb-4 font-bold text-2xl text-primary-color ">
          {' '}
          Sign In
        </h2>

        <label className="text-primary-color">Name</label>
        <input
          type="text"
          value={formik.values.name}
          name="name"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          placeholder="Enter name"
          className="rounded-[4px] p-2 mb-1 focus:ring  text-placeHolder"
        />

        {formik.errors.name && formik.touched.name && (
          <span className="text-[#FF8585] text-sm mb-1">
            {formik.errors.name}
          </span>
        )}

        <label className="text-primary-color">Email</label>
        <input
          type="text"
          placeholder="Enter E-mail "
          name="email"
          value={formik.values.email}
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          className="rounded-[4px] p-2 mb-1 focus:ring  text-placeHolder"
        />

        {formik.errors.email && formik.touched.email && (
          <span className="text-[#FF8585] text-sm mb-1">
            {formik.errors.email}
          </span>
        )}

        <label className="text-primary-color justify-between flex">
          Password
        </label>
        <input
          type="password"
          placeholder="Enter Password"
          name="password"
          value={formik.values.password}
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          className="rounded-[4px] p-2 focus:ring text-placeHolder"
        />
        {formik.errors.password && formik.touched.password && (
          <span className="text-[#FF8585] text-sm mb-1">
            {formik.errors.password}
          </span>
        )}

        <br />
        <div className="flex justify-between">
          <Button
            title="Submit"
            disabled={!formik.isValid || !formik.dirty}
            style={{
              opacity: !formik.isValid || !formik.dirty ? 0.5 : 1,
              width: '48%',
            }}
          />
          <Button
            title={<BackIcon />}
            onClick={() => {
              navigate('/')
            }}
            style={{ background: 'orange', width: '48%' }}
          />
        </div>
      </form>
    </Card>
  )
}

export default SignUp
