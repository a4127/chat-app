import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Card from '../common/Card'
import moment from 'moment'
import { io } from 'socket.io-client'
import Button from '../common/Button'
import axios from 'axios'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import {
  BASE_URL,
  GET_MESSAGES,
  GET_ROOM,
  SEND_MESSAGE,
  USER_API,
} from '../../utils/constant'
import ChatSidebar from '../../component/ChatSidebar'
import Swal from 'sweetalert2'
const socket = io.connect('http://localhost:5001')

const ChatBox = () => {
  const [chatData, setChatData] = useState<any>([])

  let userInfo = localStorage.getItem('userInfo')
  const userData = userInfo ? JSON.parse(userInfo) : ''

  const input = useRef(null)
  // const { receiverId } = useSelector((state: any) => state.chat)

  const [messageInput, setMessageInput] = useState('')

  const [receiverName, setReceiverName] = useState('')

  const [receiverId, setReceiverId] = useState()

  const [showChat, setShowChat] = useState(false)

  useEffect(() => {
    const handleReceiveMessage = (data) => {
      console.log('this is receive_message', data)
      setChatData((prevData) => [
        ...prevData,
        {
          message: data.data.data.message,
          senderId: data.data.data.senderId,
          receiverId: data.data.data.receiverId,
        },
      ])
    }

    socket.on('receive_message', handleReceiveMessage)

    // Clean up the event listener when the component is unmounted
    return () => {
      socket.off('receive_message', handleReceiveMessage)
    }
  }, [socket, setChatData])

  const handleChange = (e) => {
    console.log(e.key)
    setMessageInput(e.target.value)
  }

  const sendMessage = async () => {
    if (input) {
      const data = {
        message: messageInput,
        senderId: userData?._id,
        receiverId: receiverId,
      }

      const res = await axios.post(
        `${BASE_URL}${USER_API}${SEND_MESSAGE}`,
        data,
      )

      if (!res.data.error) {
        socket.emit('send_message', { data: data })

        // setChatData((prevData) => [
        //   ...prevData,
        //   {
        //     message: messageInput,
        //     senderId: userData?._id,
        //     receiverId: receiverId,
        //   },
        // ])
      }

      setMessageInput('')
    }
  }

  const onHandleSubmitForm = async (values: any,{ resetForm }) => {
    try {
      const data = {
        message: values.message,
        senderId: userData?._id,
        receiverId: receiverId,
      }
      const res = await axios.post(
        `${BASE_URL}${USER_API}${SEND_MESSAGE}`,
        data,
      )
  
      if (!res.data.error) {
        socket.
        emit('send_message', { data: data })
        
      }

      resetForm();

    } catch (error:any) {
      if (error.response.status == 400 || error.response.status == 401) {
        const showValidationError =
          error.response.status == 401
            ? error?.response?.data?.errors[0].constraints.error
            : error?.response?.data?.message

        Swal.fire({
          icon: 'error',
          text: showValidationError,
          timer: 1500,
        })
      } else {
        Swal.fire({
          icon: 'error',
          text: 'Something went wrong',
          timer: 1500,
        })
      }
    }
  }

  const validationSchema = Yup.object().shape({
    message: Yup.string().required('Message is required!'),
  })

  const formik = useFormik({
    initialValues: {
      message: '',
    },
    validationSchema: validationSchema,
    onSubmit: onHandleSubmitForm,
  })

  const openChat = async (friendId, name) => {
    setReceiverName(name)
    setReceiverId(friendId)

    axios
      .get(`${BASE_URL}${USER_API}${GET_ROOM}/${userData._id}/${friendId}`)
      .then((res) => {
        setShowChat(true)
        if (!res.data.error) {
          socket.emit('join_room', { roomId: res.data.data._id })
        }
      })
      .catch((err) => {
        Swal.fire('error', 'Something went wrong', 'error')
      })

    axios
      .get(`${BASE_URL}${USER_API}${GET_MESSAGES}/${userData._id}/${friendId}`)
      .then((res) => {
        setShowChat(true)
        setChatData(res?.data?.data)
      })
      .catch((err) => {
        Swal.fire('error', 'Something went wrong', 'error')
      })
    setShowChat(true)
  }

  const handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault()
      sendMessage()
    }
  }

  return (
    <React.Fragment>
      <div className="flex flex-row h-screen">
        <ChatSidebar openChat={openChat} />

        <div className="w-5/6">
          <Card style={{ width: '100%', margin: 0, background: '#7FFFD4' }}>
            {' '}
            {receiverName}
          </Card>

          <div className="h-[calc(100vh-158px)] pt-2  overflow-y-auto ">
            {chatData?.map((item) => (
              <Card
                style={{
                  marginBottom: 4,
                  marginLeft: userData._id == item.senderId ? 'auto' : '',
                  width: 'fit-content',
                  maxWidth: '25%',
                  background: '#7FFFD4',
                }}
              >
                {item.message} <br />
                {moment(item.sentAt).format('ddd, h:mm a')}
              </Card>
            ))}
          </div>

          {receiverName.length > 0 && (
            <Card style={{ width: '99%' }}>
              <form
                className="flex  flex-wrap w-[100%]"
                onSubmit={formik.handleSubmit}
              >
                <input
                  type="text"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.message}
                  name="message"
                  placeholder="Enter message"
                  className="rounded-[4px] p-2 w-[90%]  focus:ring text-placeHolder"
                />

                <Button
                  title="Send"
                  style={{
                    width: '10%',
                    position: 'relative',
                    bottom: '3px',
                    borderRadius: 0,
                  }}
                />

                {formik.errors.message && formik.touched.message && (
                  <span className="text-[#FF8585] text-sm">
                    {formik.errors.message}
                  </span>
                )}
              </form>
            </Card>
          )}
        </div>
      </div>
    </React.Fragment>
  )
}

export default ChatBox
