import React, { ReactNode, useState } from 'react'
import Sidebar from '../../component/Sidebar'
import Header from '../../component/Header'

import { Divider, Select } from 'antd'
import ChatInput from './ChatInput'
import ChatBox from './ChatBox'
import { useDispatch, useSelector } from 'react-redux'
import axios from 'axios'
import { BASE_URL,  GET_MESSAGES, USER_API } from '../../utils/constant'
import moment from 'moment'
import Card from '../common/Card'
import { useNavigate } from 'react-router'
import ProfileDetail from '../Profile/ProfileDetail'

interface ChatLayoutProps {
  children?: ReactNode
  title?: string
}



const ChatLayout: React.FC<ChatLayoutProps> = ({ children, ...props }) => {


    const [receiverName,setReceiverName] = useState('')
    const [isShow,setIsShow] = useState(false)

    const [showProfile,setShowProfile] = useState(false)


    const navigate = useNavigate()

    let userInfo = localStorage.getItem('userInfo')
    const userData = userInfo ? JSON.parse(userInfo) : ''

    const {isChatVisible} = useSelector((state:any)=>state.chat)


   


  return (
    <div className=" flex flex-row ">
     
     <Sidebar  setShowProfile={setShowProfile}  showProfile={showProfile} />
     
      <div className="w-5/6  bg-primary-color-100">

  {children}
        {/* <ChatInput emitSocket={emitSocket}/> */}
      </div>
    </div>
   
  )
}

export default ChatLayout
