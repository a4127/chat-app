import React, { ReactNode } from 'react'

interface CardProps {
  children: ReactNode
  style?: React.CSSProperties
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const Card: React.FC<CardProps> = ({ children,style,onClick }) => {
  return <div className=" bg-[#f5f5f5] flex align-middle p-4 sm:w-1/4  w-[100%] mx-2" style={style} onClick={onClick}>
    {children}
  </div>
}

export default Card
