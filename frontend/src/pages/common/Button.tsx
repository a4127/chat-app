import React from 'react'

interface ButtonProps {
  title: string | any
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
  type?: any
  style?: React.CSSProperties
  disabled?: boolean
}
const Button: React.FC<ButtonProps> = ({ title, onClick, type, style , disabled }) => {
  return (
    <button type={type} style={style} disabled={disabled} onClick={onClick} className='bg-primary-color mt-1 text-white py-2 rounded-xl hover:ring '>
      {title}
    </button>
  )
}

export default Button
