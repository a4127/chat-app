import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: null,
  error: false,
  isLoggedIn: false,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    loginSuccess: (state, action) => {
      state.user = action.payload.user;
      state.error = false;
      state.isLoggedIn = true
    },

    loggedIn :(state)=>{

  },
    loginFailure: (state ) => {
      state.user = null;
      state.error = true;
      state.isLoggedIn = false
    },


    handleCreateUserSuccess: (state, action) => {},

    handleCreateUserFailure: (state, action) => {},
    logoutHandler: (state) => {
      state.user = null;
      state.isLoggedIn = false
    },

    
  },
});

export const { loginSuccess, loginFailure ,loggedIn,logoutHandler, handleCreateUserSuccess , handleCreateUserFailure } = authSlice.actions;

export default authSlice.reducer;
