import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  receiverId: null,
  isChatVisibile:false
};

const chatSlice = createSlice({
  name: "chat",
  initialState,
  reducers: {
    getSenderId: (state, action) => {
      state.receiverId = action.payload
    },

    showChat: (state, action) => {
      return {
        ...state,
        isChatVisible: action.payload
      };
    },



    
  },
});

export const { getSenderId ,  showChat} = chatSlice.actions;

export default chatSlice.reducer;
