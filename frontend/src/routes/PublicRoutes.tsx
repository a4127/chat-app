import { Route, Routes, createBrowserRouter } from "react-router-dom";
import Dashboard from "../pages/Dashboard/ChatLayout";
import Layout from "../pages/Auth/Layout";
import React from "react";
import ChatLayout from "../pages/Dashboard/ChatLayout";
import ProfileDetail from "../pages/Profile/ProfileDetail";
import ChatBox from "../pages/Dashboard/ChatBox";
import { Navigate } from "react-router";


const PublicRouter = () => {
  return (
    <Routes>
     <Route path="/chat"  element={<ChatLayout ><ChatBox  /> </ChatLayout>} />
     <Route path="/" element={<ChatLayout ><ProfileDetail/></ChatLayout>} />
     <Route path="*" element={<Navigate to="/" />} />
    </Routes>
  );
};

export default PublicRouter;
