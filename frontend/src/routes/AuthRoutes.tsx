import { Route, Routes, createBrowserRouter } from "react-router-dom";
import Layout from "../pages/Auth/Layout";
import React from "react";
import Login from "../pages/Auth/Login";
import ForgotPassword from "../pages/Auth/ForgotPassword";
import SignUp from "../pages/Auth/SignUp";
import OtpVerification from "../pages/Auth/OtpVerification";
import ChatLayout from "../pages/Dashboard/ChatLayout";
import { Navigate } from "react-router";
import PasswordReset from "../pages/Auth/PasswordReset";



const PublicRouter = () => {
  return (
    <Routes>
    <Route path="/" element={<Layout ><Login /></Layout>} />
    <Route path="/forgot-password" element={<Layout ><ForgotPassword /></Layout>} />
    <Route path="/sign-up" element={<Layout ><SignUp /></Layout>} />
    <Route path="/verify-otp" element={<Layout ><OtpVerification /></Layout>} />
    <Route path="/password-reset/:id" element={<Layout ><PasswordReset /></Layout>} />
    <Route path="*" element={<Navigate to="/" />}/>
    </Routes>
  );
};

export default PublicRouter;
