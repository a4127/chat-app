import { useEffect, useState } from 'react'

import Header from './Header'
import { Link, useNavigate } from 'react-router-dom'
import { logoutHandler } from '../store/slice/authSlice'
import { useDispatch, useSelector } from 'react-redux'
import Swal from 'sweetalert2'
import React from 'react'
import { Divider, Select } from 'antd'
import Card from '../pages/common/Card'
import Button from '../pages/common/Button'
import axios from 'axios'
import {
  BASE_URL,
  GET_ALL_FRIENDS,
  GET_ALL_USERS,
  USER_API,
} from '../utils/constant'
import { getSenderId, showChat } from '../store/slice/chatSlice'

interface SidebarProps {
  setShowProfile: (e: string, name: string) => void
  showProfile: boolean
}

export default function Sidebar<SidebarProps>({
  setShowProfile,
  showProfile,
}) {
  const [open, setOpen] = useState(false)
  const dispatch = useDispatch()
 
 

  const [isChatVisible, setIsChatVisible] = useState(false)

  // const {isChatVisible} = useSelector((state:any)=>state.chat)
  const handleLogOut = () => {
    Swal.fire({
      icon: 'success',
      html: 'You have been logged out',
      timer: 2000,
    }).then(() => {
      dispatch(logoutHandler())
      localStorage.clear()
      navigate('/')
    })
  }

  let userInfo = localStorage.getItem('userInfo')
  const userData = userInfo ? JSON.parse(userInfo) : ''
  const navigate = useNavigate()


  return (
    <div className="w-1/6 bg-[#f5f5f5] p-4">
  

    
        <div className="text-center text-2xl">Chat Application</div>

        <div className="mt-8">
          <Button
            title="Chat"
            style={{ width: '100%' }}
            onClick={() => {
              dispatch(showChat(true))
              navigate('/chat')
            }}
          />


          <Button
            title="Profile"
            style={{ width: '100%' }}
            onClick={() => {
              // setShowProfile(true)
              dispatch(showChat(false))
              navigate('/')
            }}
          />
        </div>

        <div className="" style={{ marginTop: 710 }}>
          <Button
            title="logout"
            style={{ width: '100%' }}
            onClick={handleLogOut}
          />
        </div>
            
    </div>
  )
}
