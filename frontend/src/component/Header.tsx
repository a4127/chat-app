import { Badge, Button, Tooltip } from 'antd'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { loginFailure, logoutHandler } from '../store/slice/authSlice'
import { BASE_URL} from '../utils/constant'
import Swal from 'sweetalert2'
import axios from 'axios'

interface HeaderProps {
  title?: string
}

const Header: React.FC<HeaderProps> = ({ title, ...props }) => {
  // console.log(props)

  var user = localStorage.getItem('userData')

  const dispatch = useDispatch()

  const handleLogOut = () => {
    Swal.fire({
      icon: 'success',
      html: 'You have been logged out',
      timer: 2000,
    }).then(() => {
      dispatch(logoutHandler())
      localStorage.clear()
    })
  }

  const navigate = useNavigate()

  const handleNavigate = () => {
    if (user) userId = JSON.parse(user)

    axios
      .get(`${BASE_URL}dummy/getCartItems/${userId?._id}`, {
        headers: {
          Authorization: localStorage.getItem('token')
            ? `Bearer ${localStorage.getItem('token')}`
            : null,
        },
      })
      .then((response: any) => {
        console.log(response.data)
      })
      .catch((error) => {
      
         Swal.fire("","Something went wrong","error")
      })

    navigate('/cart')
  }

  let userId: any

  React.useEffect(() => {
    if (user) userId = JSON.parse(user)

    axios
      .get(`${BASE_URL}dummy/getCount/${userId?._id}`, {
       
      })
      .then((response) => {
        // if (!response.data.error) dispatch(cartCountSuccess(response.data.data))
        // else dispatch(cartCountFailure(response.data.error))
      })
      .catch((error) => {
        console.log(error)
        Swal.fire('', 'Something went wrong', 'error')
      })
  }, [])

//   const { count } = useSelector((state: any) => state.cart)

  return ( 
    <div className="flex p-3 justify-between bg-[#f5f5f5]">
      <h3>{title}</h3>
      <div className="flex gap-3">
        <Tooltip placement="bottom" title={'Cart'}>
          <Badge
            count={'2'}
            style={{ height: 15, width: 5, backgroundColor: 'green' }}
          >
            <nav>
              <i
                className={`fa-solid fa-cart-shopping cursor-pointer mt-1 hover:opacity-50`}
                onClick={handleNavigate}
              ></i>{' '}
            </nav>
          </Badge>
        </Tooltip>

        <Tooltip placement="bottomLeft" title={'log out'}>
          <nav>
            <i
              className={`fa-solid fa-power-off cursor-pointer hover:opacity-50`}
              onClick={handleLogOut}
            ></i>{' '}
          </nav>
        </Tooltip>
      </div>
    </div>
  )
}

export default Header
