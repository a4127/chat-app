import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { BASE_URL, GET_ALL_FRIENDS, USER_API } from '../utils/constant'
import Swal from 'sweetalert2'
import Card from '../pages/common/Card'

const ChatSidebar = ({openChat}) => {
  let userInfo = localStorage.getItem('userInfo')
  const userData = userInfo ? JSON.parse(userInfo) : ''

  const [friendList, setFriendList] = useState([])
  const getFriendList = () => {
    axios
      .get(`${BASE_URL}${USER_API}${GET_ALL_FRIENDS}/${userData._id}`)
      .then((res) => {
        if (res.status === 200) {
          setFriendList(res?.data?.data?.friend ? res?.data?.data?.friend : [])

          // setFriend()
        } else {
          Swal.fire('error', 'Something went wrong', 'error')
        }
      })
  }

  useEffect(() => {
    getFriendList()
  }, [])

  return (
    <div className="w-1/6 bg-[#f5f5f5] shadow-2xl  p-4">
      <div className=" overflow-y-auto ">
        <div className="text-center text-2xl">Friend List</div>

        <div className="mt-8">
          {friendList?.map((item: any) => (
            <Card
              style={{
                background: '#7FFFD4',
                width: '100%',
                margin: 0,
                borderRadius: 4,
                marginBottom: 2,
                cursor: 'pointer',
              }}

                onClick={() => { openChat(item?.friendId?._id, item?.friendId?.name) }}
            >
              {item?.friendId?.name}
            </Card>
          ))}
        </div>
      </div>
    </div>
  )
}

export default ChatSidebar
