export const BASE_URL = "http://localhost:5001/"
export const AUTH_API = "api/auth/"
export const USER_API = "api/user/"
export const LOGIN = "login"
export const GET_ALL_USERS = "getAllUsers"
export const GET_ALL_FRIENDS = "getAllFriends"
export const SEND_MESSAGE = "sendMessage"
export const GET_MESSAGES = "getMessages"
export const GET_ROOM = "getRoom"


