import mongoose, { Schema, Document, Model, Types } from 'mongoose';

interface friend {
  userId:Types.ObjectId,
  friend: Array<{friendId:Types.ObjectId}>
   
}

interface FriendDocument extends friend, Document {}

const FriendSchema = new Schema<FriendDocument>(
  {
    userId: { type: mongoose.Schema.Types.ObjectId, ref: "user", required: true },
    friend : [{
        friendId:{ type: mongoose.Schema.Types.ObjectId,ref: "user", required: true },
    }]
    
  },
  { strict: false, versionKey: false, autoIndex: true }
);

const FriendModel: Model<FriendDocument> = mongoose.model<FriendDocument>('friend', FriendSchema);

export default FriendModel;
