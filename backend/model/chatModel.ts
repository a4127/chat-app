import mongoose, { Schema, Document, Model, Types } from 'mongoose'

interface chat {
  senderId: Types.ObjectId
  receiverId: Types.ObjectId
  message: String
  sentAt: Date
}

interface ChatDocument extends chat, Document {}

const ChatSchema = new Schema<ChatDocument>(
  {
    senderId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
      required: true,
    },
    receiverId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
      required: true,
    },
    message: { type: String },
    sentAt: { default: Date.now(), type: Date },
  },
  { strict: false, versionKey: false, autoIndex: true },
)

const ChatModel: Model<ChatDocument> = mongoose.model<ChatDocument>(
  'chat',
  ChatSchema,
)

export default ChatModel
