import mongoose, { Schema, Document, Model } from 'mongoose';

interface User {
  name: string;
  email: string;
  password: string;
  isVerified:boolean;
  otp:number,
  expired_at:Date
}

interface UserDocument extends User, Document {}

const UserSchema = new Schema<UserDocument>(
  {
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    isVerified:{type:Boolean,default:false},
    otp:{type:Number},
    expired_at:{type:Date}
  },
  { strict: false, versionKey: false, autoIndex: true }
);

const UserModel: Model<UserDocument> = mongoose.model<UserDocument>('user', UserSchema);

export default UserModel;
