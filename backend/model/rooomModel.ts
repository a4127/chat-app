import mongoose, { Schema, Document, Model, Types } from 'mongoose';

interface Room {
  createdId:Types.ObjectId;
  friendId:Types.ObjectId
  
}

interface RoomDocument extends Room, Document {}

const RoomSchema = new Schema<RoomDocument>(
  {
    createdId: { type: mongoose.Schema.Types.ObjectId, ref: "user", required: true },
    friendId: { type: mongoose.Schema.Types.ObjectId, ref: "user", required: true },
  },
  { strict: false, versionKey: false, autoIndex: true }
);

const RoomModel: Model<RoomDocument> = mongoose.model<RoomDocument>('room', RoomSchema);

export default RoomModel;
