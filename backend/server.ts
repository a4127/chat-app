
import express from 'express';
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import cors from 'cors'
import authRoutes from './routes/authRoutes'
import userRoutes from './routes/userRoutes'
import { Server } from 'socket.io';
import http from 'http'
import { handleSocketConnection } from './services/socketServices';

dotenv.config();
const app = express()
const server = http.createServer(app)

app.use(express.json());
app.use(cors());

const DB_URL = process.env.MONGO_DB_URL;


const io = new Server(server,{
  cors:{
    origin:"http://localhost:3000"
  }
})


handleSocketConnection(io);

if(DB_URL){
  mongoose.connect(DB_URL)
}

const db = mongoose.connection;



app.use('/api/auth',authRoutes)
app.use('/api/user',userRoutes)

app.use((error:any, req:any, res:any, next:any) => {
  res.status(500).send("Something broke!");
});

server.listen(process.env.PORT, () => {
  console.log("Server listening on PORT", process.env.PORT);
});




db.on("error", console.error.bind(console, "MongoDB connection error:"));
db.once("open",  () => {
  console.log("Connected to MongoDB");
});



