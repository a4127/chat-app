import { IsEmail, IsNotEmpty, Length, Min } from "class-validator";

export class loginValidator {
    @IsNotEmpty({message:"Email is required"})
    @IsEmail()
    email!: string; 

    @IsNotEmpty({message:"Password is required"})
    @Length(4, 255, { message: 'Password must be at least 4 characters' })
    password!: string; 
}