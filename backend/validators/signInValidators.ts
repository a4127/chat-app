import { IsEmail, IsNotEmpty, Length } from "class-validator";

export class signInValidator {
  @IsNotEmpty()
  name! :string

  @IsNotEmpty({message:"Email is required"})
  @IsEmail({}, { message: 'Email is not valid' })
  email!: string; 

  @IsNotEmpty({message:"Password is required"})
  @Length(4, 255, { message: 'Password must be at least 4 characters' })
  password!: string; 

}