import {  IsNotEmpty, Length } from "class-validator";

export class messageValidator {
    @IsNotEmpty({message:"Message must not be empty"})
    message!: string; 


    @IsNotEmpty({message:"Receiver is required"})
    receiverId!: string; 

    @IsNotEmpty({message:"Sender is required"})
    senderId!: string

}