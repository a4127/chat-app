import {  IsNotEmpty, Length } from "class-validator";

export class resetPasswordValidator {
    @IsNotEmpty({message:"Password is required"})
    @Length(4, 255, { message: 'Password must be at least 4 characters' })
    password!: string; 

    @IsNotEmpty({message:"Id is required"})
    id!: string; 
}