import { Request, Response, NextFunction } from 'express'
import User from '../model/userModel'
import { message } from 'antd'
import Friend from '../model/friendModel'
import Chat from '../model/chatModel'
import Room from '../model/rooomModel'
import { messageValidator } from '../validators/messageValidator'
import { ValidationError, validate } from 'class-validator'
const getAllUsers = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const userDetails = await User.find({ isVerified: true })

    return res.status(200).send({
      error: false,
      message: 'User fetched successfully',
      data: userDetails,
    })
  } catch (error) {
    return res
      .status(400)
      .send({ error: true, message: 'Unable to fetch users' })
  } 
}

const getAllFriends = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ) => {
    try {
      const userId = req.params.userId
  
      const user = await Friend.findOne({ userId })
        .populate('friend.friendId')
        .lean()
  
      return res
        .status(200)
        .send({
          error: false,
          message: 'Friends fetched successfully',
          data: user,
        })
    } catch (error) {
      return res
        .status(400)
        .send({ error: true, message: 'Unable to fetch the friends' })
    }
  
    //    const friendDocument = await Friend.findOne({ userId }).populate('friend.friendId');
  }


  const getRoom = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ) => {
    try {
      const senderId = req.params.senderId
      const receiverId = req.params.receiverId

      const chat = await Room.findOne({ $or :[{ createdId:senderId , friendId:receiverId} , { createdId:receiverId , friendId:senderId}] })
        .lean()
  
      return res
        .status(200)
        .send({
          error: false,
          message: 'Room fetched successfully',
          data: chat,
        })
    } catch (error) {
      return res
        .status(400)
        .send({ error: true, message: 'Unable to fetch room' })
    }
  
    //    const friendDocument = await Friend.findOne({ userId }).populate('friend.friendId');
  }
  

  const getMessages = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ) => {
    try {
      const senderId = req.params.senderId
      const receiverId = req.params.receiverId

      const chat = await Chat.find({ $or :[{ senderId:senderId , receiverId:receiverId} , { senderId:receiverId , receiverId:senderId}] })
        .lean()
  
      return res
        .status(200)
        .send({
          error: false,
          message: 'Chat fetched successfully',
          data: chat,
        })
    } catch (error) {
      return res
        .status(400)
        .send({ error: true, message: 'Unable to fetch chat' })
    }
  
    //    const friendDocument = await Friend.findOne({ userId }).populate('friend.friendId');
  }
  
  

const addFriend = async (req: Request, res: Response, next: NextFunction) => {
  const { userId, friendId } = req.body

  try {

    const isRoomCreated = await Room.findOne({  $or:[{createdId:userId, friendId:friendId} , {createdId:friendId, friendId:userId}]})

    if(!isRoomCreated){
      const createRoom = await new Room ({
          createdId: userId,
          friendId: friendId
      })
      await createRoom.validate()
      await createRoom.save()
    }
    const userEntry = await Friend.findOne({ userId })

    if (userEntry) {
      await Friend.findOneAndUpdate(
        { userId }, // filter operation
        { $push: { friend: { friendId } } }, // here i am pushing the element if the entry is found
        { new: true },
      )
      return res
        .status(201)
        .send({ error: false, message: 'User friend updated successfully' })
    } else {
      const createFriend = new Friend({
        userId,
        friend: [{ friendId }],
      })

      await createFriend.validate()
      await createFriend.save()
      if (createFriend)
        return res
          .status(201)
          .send({ error: false, message: 'User and friend added successfully' })
    }
  } catch (error) {
    return res
      .status(400)
      .send({ error: true, message: 'Fail to add a friend' })
  }
}

const removeFriend = async (req: Request, res: Response, next: NextFunction) => {
  const { userId, friendId } = req.body

  try {

    const userEntry = await Friend.findOne({ userId })

    if (userEntry) {
      await Friend.findOneAndUpdate(
        { userId }, // filter operation
        {  $pull: { friend: { friendId } } }, // here i am removing the element if the entry is found
        { new: true },
      )
      
      // here i am deleting the chat also 
      const chatToDelete  = await Chat.deleteMany({$or :[{senderId:userId , receiverId:friendId} , {senderId:friendId , receiverId:userId} ]})

   
      return res
        .status(201)
        .send({ error: false, message: 'Friend remove successfully' })
    } 
  } catch (error) {
    return res
      .status(400)
      .send({ error: true, message: 'Fail to remove a friend' })
  }
}



const sendMessage = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { senderId, message, receiverId } = req.body

    const messageDto = new messageValidator();
    Object.assign(messageDto,  req.body); 
  
    const errors: ValidationError[] = await validate(messageDto);
  
    if (errors.length > 0) {
      const validationMessage = errors.map((item:any) => {
        const constraints = Object.keys(item.constraints).reduce((acc:any, key) => {
          // Assign a fixed key name "error" for all constraints
          acc["error"] = item.constraints[key];
          return acc;
        }, {});
      
        return {
          property: item.property,
          constraints: constraints,
        };
      });
      return res.status(401).json({ errors:validationMessage })
    }
  


    const addMessage = await new Chat({
      senderId,
      message,
      receiverId,
      sentAt: new Date(),
    })

    await addMessage.validate()
    await addMessage.save()


    return res
      .status(200)
      .send({
        error: false,
        message: 'Message added successfully',
        // data: ,
      })
  } catch (error) {
    return res
      .status(400)
      .send({ error: true, message: 'Unable to add the message' })
  }

  //    const friendDocument = await Friend.findOne({ userId }).populate('friend.friendId');
}

export { getAllUsers, addFriend, getAllFriends, sendMessage,getMessages , removeFriend , getRoom }
