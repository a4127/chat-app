import { Request, Response, NextFunction } from 'express'

import User from '../model/userModel'

const nodemailer = require('nodemailer')

var smtpTransport = require('nodemailer-smtp-transport')


import { ValidationError, validate } from 'class-validator'
import { loginValidator } from '../validators/loginValidators'
import { signInValidator } from '../validators/signInValidators'
import { resetPasswordValidator } from '../validators/resetPasswordValidator'


const createUser = async (req: Request, res: Response, next: NextFunction) => {
  const { email, name, password } = req.body


  const signInDto = new signInValidator();
  Object.assign(signInDto,  req.body); 

  const errors: ValidationError[] = await validate(signInDto);


  if (errors.length > 0) {
    const validationMessage = errors.map((item:any) => {
      const constraints = Object.keys(item.constraints).reduce((acc:any, key) => {
        // Assign a fixed key name "error" for all constraints
        acc["error"] = item.constraints[key];
        return acc;
      }, {});
    
      return {
        property: item.property,
        constraints: constraints,
      };
    });

    return res.status(401).json({ errors:validationMessage });
  }



  try {
    const exitedUser = await User.findOne({ email: email })

    if (exitedUser) {
      return res
        .status(202)
        .send({ error: false, message: 'User already exist' })
    }
    const otp = Math.floor(1000 + Math.random() * 9000)

    const user = new User({
      email,
      name, 
      password,
      isVerified: false,
      otp,
      expired_at: new Date(Date.now() + 5 * 60 * 1000),
    })

    await user.validate()
    await user.save()

    if (user) {
      const transporter = nodemailer.createTransport(
        smtpTransport({
          service: 'gmail',
          host: 'smtp.gmail.com',
          secure: false,
          auth: {
            user: process.env.NODEMAILER_EMAIl,
            pass: process.env.NODEMAILER_PASSWORD,
          },
        }),
      )

      const mailOptions = {
        from: process.env.NODEMAILER_EMAIl,
        to: email,
        subject: 'Otp verification',
        html: `Please note you otp is ${user.otp}`,
      }

      transporter.sendMail(mailOptions, function (error: any) {
        if (error) {
          console.log(error)
        } else {
          return res.status(201).json({
            message: 'Otp sent successfully to your email id',
            data: user.email,
          })
        }
      })
    }
  } catch (error) {
    console.log(error)

    res.status(400).send({ error: true, message: 'Failed to create the user' })
  }
}

const verifyOtp = async (req: Request, res: Response, next: NextFunction) => {
  const { email, otp } = req.body

  const verifyUser = await User.findOne({ email })

  try {
    if (verifyUser) {
      if (verifyUser.expired_at > new Date(Date.now())) {
        if (verifyUser.otp == otp) {
          const data = await verifyUser.updateOne({ isVerified: true })

          return res
            .status(200)
            .send({ error: false, message: 'User has been verified' })
        } else {
          return res
            .status(400)
            .send({ error: false, message: 'otp do not match' })
        }
      } else {
        return res
          .status(202)
          .send({ error: false, message: 'otp has been expired' })
      }
    }
  } catch (error) {
    return res
      .status(400)
      .send({ error: true, message: 'Something went wrong' })
  }
}

const handleForgotPassword = async (req:Request, res:Response, next:NextFunction): Promise<any> => {
  const { email } = req.body;

   try {


    const userId = await User.findOne({email})


  if(!userId) {
    throw res.status(400).json({message:"Email not exist"})
   }

  // implemented because we need to expire the link also and handling this in frontend
  // const token = jwt.sign({ email, exp: Math.floor(Date.now() / 1000) + (60) }, process.env.SECRET_KEY);


  const transporter = nodemailer.createTransport(
    smtpTransport({
      service: 'gmail',
      host: 'smtp.gmail.com',
      secure: false,
      auth: {
        user: process.env.NODEMAILER_EMAIl,
        pass: process.env.NODEMAILER_PASSWORD,
      },
    }),
  )

  
  const mailOptions = {
    from: process.env.NODEMAILER_EMAIl,
    to: email, 
    subject: 'Reset Password',
    html: `<a href="http://localhost:3000/password-reset/${userId._id}">Please click to reset the password</a>`

  };  

  transporter.sendMail(mailOptions, function(error:any){
    if (error) {
      console.log(error);
    } else {
      return res.status(200).json({message:"Email sent successfully"})
    }
  });


    
   } catch (error) {
    console.log(error)
   }
 
}


const handlePasswordReset = async (req:Request, res:Response, next:NextFunction): Promise<any> => {
  const { id , password } = req.body;

  const resetPasswordDto = new resetPasswordValidator();
  Object.assign(resetPasswordDto,  req.body); 

  const errors: ValidationError[] = await validate(resetPasswordDto);

  if (errors.length > 0) {
    const validationMessage = errors.map((item:any) => {
      const constraints = Object.keys(item.constraints).reduce((acc:any, key) => {
        // Assign a fixed key name "error" for all constraints
        acc["error"] = item.constraints[key];
        return acc;
      }, {});
    
      return {
        property: item.property,
        constraints: constraints,
      };
    });

    return res.status(401).json({ errors:validationMessage });
  }

  let user:any = await User.findOne({ _id:id });

  if(user) {
    const validPassword = password == password

    if(validPassword){
      user.password = password;
      await user.save();
    }

    res.status(200).json({ success: true, message: "Password updated successfully." });
  } else {
    res.status(404).json({ success: false, message: "User not found." });
  }


 
};

const loginUser = async (req: Request, res: Response, next: NextFunction) => {

  const loginDto = new loginValidator();
  Object.assign(loginDto,  req.body); 

  const errors: ValidationError[] = await validate(loginDto);


  if (errors.length > 0) {
    const validationMessage = errors.map((item:any) => {
      const constraints = Object.keys(item.constraints).reduce((acc:any, key) => {
        // Assign a fixed key name "error" for all constraints
        acc["error"] = item.constraints[key];
        return acc;
      }, {});
    
      return {
        property: item.property,
        constraints: constraints,
      };
    });

    return res.status(401).json({ errors:validationMessage });
  }

  const { email, password } = req.body

  let data = await User.findOne({ email }).lean()

  try {


  if (data) {
    const validPassword = password == data.password
    if (validPassword) {
      return res
        .status(200)
        .json({ message: 'Login successful', error: false, data: data })
    } else {
      throw res.status(400).json({ message: 'Password not match', error: true })
    }
  } else {
    throw res.status(400).json({ message: 'Email not found', error: true })
  }

    
  } catch (error) {
    
  }
}

export { createUser, verifyOtp, loginUser,handleForgotPassword,handlePasswordReset}
