import express from 'express'
import { getAllUsers , addFriend , getAllFriends , sendMessage , getMessages , removeFriend,getRoom} from '../controller/userController'

const router = express.Router()

router.get('/getAllUsers',getAllUsers)
router.post('/addFriend',addFriend)
router.post('/removeFriend',removeFriend)
router.get('/getAllFriends/:userId',getAllFriends)
router.post('/sendMessage',sendMessage)
router.get('/getMessages/:senderId/:receiverId',getMessages)

router.get('/getRoom/:senderId/:receiverId',getRoom)

export default router