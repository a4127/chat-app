import express from 'express'
import {createUser,loginUser,verifyOtp , handleForgotPassword , handlePasswordReset} from '../controller/authController'
const router = express.Router()

router.post('/login',loginUser)
router.post('/create',createUser)
router.post('/verify-otp',verifyOtp)
router.post('/forgot-password',handleForgotPassword)
router.post('/password-reset',handlePasswordReset);

export default router