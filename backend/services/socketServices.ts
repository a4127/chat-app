// socketController.ts

import { Server, Socket } from 'socket.io';

export const handleSocketConnection = (io: Server) => {
  io.on('connection', (socket: Socket) => {
    let currentRoom: string | null = null;

    socket.on('join_room', (data) => {
      console.log('User joined room:', data.roomId);

      // Leave the current room, if any
      if (currentRoom) {
        socket.leave(currentRoom);
      }

      // Join the new room
      socket.join(data.roomId);
      currentRoom = data.roomId;
    });

    socket.on('send_message', (data) => {
      // Send the message to the specific room (currentRoom)
      if (currentRoom) {
        io.to(currentRoom).emit('receive_message', { data });
      }
    });
  });
};
